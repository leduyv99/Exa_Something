import { _API } from "@/common/utils";
import axios from "axios";

export class User {
  private context;
  private authConfig;

  constructor() {
    this.context = axios.create({ baseURL: _API });
    this.authConfig = (token: string) => ({
      headers: {
        Authorization: token,
      },
    });
  }

  async getByToken(token: string): Promise<any> {
    let data;
    try {
      const res = await this.context.get("Me", this.authConfig(token));
      data = res.data;
    } catch (err) {
      alert(err);
    }
    return data;
  }

  async getById(id: number) {
    let data;
    try {
      const res = await this.context.get(`/Props/GetUserById?userId=${id}`);
      data = res.data;
    } catch (err) {
      alert(err);
    }
    return data;
  }

  async login(params: FormData) {
    let data;
    try {
      const res = await this.context.post("Login", params);
      data = res.data;
    } catch (err) {
      alert(err);
    }
    return data;
  }

  async register(params: FormData) {
    let data;
    try {
      const res = await this.context.post("Register", params);
      data = res.data;
    } catch (err) {
      alert(err);
    }
    return data;
  }

  async logout(params: FormData) {
    let data;
    try {
      const res = await this.context.post("Logout", params);
      data = res.data;
    } catch (err) {
      alert(err)
    }
    return data;
  }
}
