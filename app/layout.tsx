import type { Metadata } from 'next'
import { Roboto } from 'next/font/google'
import Footer from '@/components/Footer/page'
import Header from '@/components/Header/page'
import Provider from '@/components/hocs/provider'
import './globals.scss'


const inter = Roboto({
  subsets: ['latin', 'vietnamese'], 
  weight: '400'
})

export const metadata: Metadata = {
  title: 'Something',
  description: 'Pnp',
}

interface Props {
  children: JSX.Element
}

export default function RootLayout({ children }: Props) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <Header />
        <Provider>
          {children}
        </Provider>
        <Footer />
      </body>
    </html>
  )
}
