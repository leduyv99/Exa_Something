export const toCapitalize = (str: string): string => {
  return str.charAt(0).toUpperCase() + str.slice(1);
} 

export const _HOST: string = 'https://www.pnp.name.vn'
export const _API: string = `${_HOST}/api`
export const _UPLOAD: string = `${_HOST}/uploader`
