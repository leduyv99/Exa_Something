'use client'

import React, { Component } from 'react'
import { Container, SSRProvider } from 'react-bootstrap'

type Props = {
  children: React.ReactNode
}

type State = {}

class Provider extends Component<Props, State> {
  state = {}

  render() {
    return (
      <SSRProvider>
        <Container>
          {this.props.children}
        </Container>
      </SSRProvider>
    )
  }
}

export default Provider