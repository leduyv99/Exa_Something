'use client';

import React, { useState } from 'react'
import { toCapitalize } from '@/common/utils';
import { Form, Stack, Button } from 'react-bootstrap'
import { globalSize } from '@/common/constraint';
import Link from 'next/link';
import { User } from '@/services/User';

type Props = {
  styles: { [key: string]: string };
}

const { Control, Group } = Form
const fields = ['username', 'password']

const HeaderForm = (props: Props) => {
  const { styles } = props
  const [useValidation, setUseValidation] = useState(false)
  const _user = new User();

  const renderField = (field: string): JSX.Element => {
    let result = <></>

    switch(field) {
      case 'password':
        result = <Control required
          name={field}
          placeholder={toCapitalize(field)}
          size={globalSize} type={field} />
        break
      default:
        result = <Control required
        name={field}
        placeholder={toCapitalize(field)}
        size={globalSize} />
    }

    return result
  }

  const onSubmit = (event: any) => {
    event.preventDefault()
    console.log(event.target)
    const formData = new FormData(event.target)
    _user.login(formData).then(res => console.log(res))
  }

  return (
    <>
      <small className={styles.register}>
        <Link href='register'>
          Register
        </Link>
      </small>
      <div className={styles.divider} ></div>
      <Form onSubmit={onSubmit} noValidate validated={useValidation}>
        <Stack direction='horizontal' gap={2}>
          {fields.map(field => 
            <Group key={field}>
            {renderField(field)}
            </Group>
          )}
          <Button variant='main' size={globalSize} type='submit'>Login</Button>
        </Stack>
      </Form>
    </>
  )
}

export default HeaderForm 