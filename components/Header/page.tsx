import React from 'react'
import styles from './styles.module.scss'
import HeaderForm from './HeaderForm'

type Props = {}

const Header = (props: Props) => {
  return (
    <div className='anchor header'>
      <div className={styles.container}>
        <div>
        </div>
        <div className={styles.right}>
          <HeaderForm styles={styles} />
        </div>
      </div>
    </div>
  )
}

export default Header